resource "aws_route53_zone" "test" {
  name = "example${random_string.this.result}.com"
}

#####
# Standard versioned example
#####

module "standard" {
  source = "../../"

  name = "tfteststandard${random_string.this.result}"

  iam_user_enabled                = true
  iam_user_access_key_enabled     = true
  iam_user_name                   = "tftest${random_string.this.result}"
  iam_user_path                   = "/test/"
  iam_user_data_access_permission = "RO"
  iam_user_tags = {
    test = "tfteststandard${random_string.this.result}"
  }

  route53_records = {
    "test" = {
      name    = "test"
      zone_id = aws_route53_zone.test.zone_id
    }

    "test2" = {
      name    = "test2"
      zone_id = aws_route53_zone.test.zone_id
      ttl     = 30
    }
  }

  kms_key_create     = true
  kms_key_name       = "tfteststandard${random_string.this.result}"
  kms_key_alias_name = "tfteststandard${random_string.this.result}"

  request_payment_configuration_payer = "Requester"

  iam_policy_create       = true
  iam_policy_output_jsons = false
  iam_policy_read_name    = "tfteststandardread${random_string.this.result}"
  iam_policy_full_name    = "tfteststandardfull${random_string.this.result}"
  iam_policy_data_ro_name = "tfteststandardro${random_string.this.result}"
  iam_policy_data_rw_name = "tfteststandardrw${random_string.this.result}"
  iam_policy_tags = {
    testTag = "tftest_override"
  }

  bucket_object_ownership = "BucketOwnerPreferred"

  versioning_configuration = {
    status     = true
    mfa_delete = false
  }

  tags = {
    testTag = "tftest"
    module  = "standard"
  }
}

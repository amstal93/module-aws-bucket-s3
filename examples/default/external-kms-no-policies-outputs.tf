#####
# S3 bucket
#####

output "external_kms_no_policies_id" {
  value = module.external_kms_no_policies.id
}

output "external_kms_no_policies_arn" {
  value = module.external_kms_no_policies.arn
}

output "external_kms_no_policies_bucket_domain_name" {
  value = module.external_kms_no_policies.bucket_domain_name
}

output "external_kms_no_policies_bucket_regional_domain_name" {
  value = module.external_kms_no_policies.bucket_regional_domain_name
}

output "external_kms_no_policies_hosted_zone_id" {
  value = module.external_kms_no_policies.hosted_zone_id
}

output "external_kms_no_policies_region" {
  value = module.external_kms_no_policies.region
}

#####
# KMS key
#####

output "external_kms_no_policies_kms_key_arn" {
  value = module.external_kms_no_policies.kms_key_arn
}

output "external_kms_no_policies_kms_key_id" {
  value = module.external_kms_no_policies.kms_key_id
}

output "external_kms_no_policies_kms_alias_arn" {
  value = module.external_kms_no_policies.kms_alias_arn
}

output "external_kms_no_policies_kms_alias_target_key_arn" {
  value = module.external_kms_no_policies.kms_alias_target_key_arn
}

#####
# Service IAM User
#####

output "external_kms_no_policies_iam_user_arn" {
  value = module.external_kms_no_policies.iam_user_arn
}

output "external_kms_no_policies_iam_user_name" {
  value = module.external_kms_no_policies.iam_user_name
}

output "external_kms_no_policies_iam_user_unique_id" {
  value = module.external_kms_no_policies.iam_user_unique_id
}

output "external_kms_no_policies_iam_user_iam_access_key_id" {
  value     = module.external_kms_no_policies.iam_user_iam_access_key_id
  sensitive = true
}

output "external_kms_no_policies_iam_user_iam_access_key_secret" {
  value     = module.external_kms_no_policies.iam_user_iam_access_key_secret
  sensitive = true
}

#####
# IAM policy
#####

output "external_kms_no_policies_iam_policy_read_only_id" {
  value = module.external_kms_no_policies.iam_policy_read_only_id
}

output "external_kms_no_policies_iam_policy_read_only_arn" {
  value = module.external_kms_no_policies.iam_policy_read_only_arn
}

output "external_kms_no_policies_iam_policy_read_only_description" {
  value = module.external_kms_no_policies.iam_policy_read_only_description
}

output "external_kms_no_policies_iam_policy_read_only_name" {
  value = module.external_kms_no_policies.iam_policy_read_only_name
}

output "external_kms_no_policies_iam_policy_read_only_json" {
  value = module.external_kms_no_policies.iam_policy_read_only_json
}

output "external_kms_no_policies_iam_policy_full_id" {
  value = module.external_kms_no_policies.iam_policy_full_id
}

output "external_kms_no_policies_iam_policy_full_arn" {
  value = module.external_kms_no_policies.iam_policy_full_arn
}

output "external_kms_no_policies_iam_policy_full_description" {
  value = module.external_kms_no_policies.iam_policy_full_description
}

output "external_kms_no_policies_iam_policy_full_name" {
  value = module.external_kms_no_policies.iam_policy_full_name
}

output "external_kms_no_policies_iam_policy_full_json" {
  value = module.external_kms_no_policies.iam_policy_full_json
}

output "external_kms_no_policies_iam_policy_data_ro_id" {
  value = module.external_kms_no_policies.iam_policy_data_ro_id
}

output "external_kms_no_policies_iam_policy_data_ro_arn" {
  value = module.external_kms_no_policies.iam_policy_data_ro_arn
}

output "external_kms_no_policies_iam_policy_data_ro_description" {
  value = module.external_kms_no_policies.iam_policy_data_ro_description
}

output "external_kms_no_policies_iam_policy_data_ro_name" {
  value = module.external_kms_no_policies.iam_policy_data_ro_name
}

output "external_kms_no_policies_iam_policy_data_ro_json" {
  value = module.external_kms_no_policies.iam_policy_data_ro_json
}

output "external_kms_no_policies_iam_policy_data_rw_id" {
  value = module.external_kms_no_policies.iam_policy_data_rw_id
}

output "external_kms_no_policies_iam_policy_data_rw_arn" {
  value = module.external_kms_no_policies.iam_policy_data_rw_arn
}

output "external_kms_no_policies_iam_policy_data_rw_description" {
  value = module.external_kms_no_policies.iam_policy_data_rw_description
}

output "external_kms_no_policies_iam_policy_data_rw_name" {
  value = module.external_kms_no_policies.iam_policy_data_rw_name
}

output "external_kms_no_policies_iam_policy_data_rw_json" {
  value = module.external_kms_no_policies.iam_policy_data_rw_json
}

output "external_kms_no_policies_iam_policy_kms_ro_json" {
  value = module.external_kms_no_policies.iam_policy_kms_ro_json
}

output "external_kms_no_policies_iam_policy_kms_data_ro_json" {
  value = module.external_kms_no_policies.iam_policy_kms_data_ro_json
}

output "external_kms_no_policies_iam_policy_kms_rw_json" {
  value = module.external_kms_no_policies.iam_policy_kms_rw_json
}

output "external_kms_no_policies_iam_policy_kms_data_rw_json" {
  value = module.external_kms_no_policies.iam_policy_kms_data_rw_json
}

terraform {
  required_version = ">= 1.1.1"

  experiments = [module_variable_optional_attrs]

  required_providers {
    aws = {
      source = "hashicorp/aws"
      # Before 4.12 gives
      # MalformedXML: The XML you provided was not well-formed or did not validate against our published schema
      version = "> 4.12"
    }
  }
}

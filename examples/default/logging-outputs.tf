#####
# S3 bucket
#####

output "logging_destination_id" {
  value = module.logging_destination.id
}

output "logging_destination_arn" {
  value = module.logging_destination.arn
}

output "logging_destination_bucket_domain_name" {
  value = module.logging_destination.bucket_domain_name
}

output "logging_destination_bucket_regional_domain_name" {
  value = module.logging_destination.bucket_regional_domain_name
}

output "logging_destination_hosted_zone_id" {
  value = module.logging_destination.hosted_zone_id
}

output "logging_destination_region" {
  value = module.logging_destination.region
}

#####
# KMS key
#####

output "logging_destination_kms_key_arn" {
  value = module.logging_destination.kms_key_arn
}

output "logging_destination_kms_key_id" {
  value = module.logging_destination.kms_key_id
}

output "logging_destination_kms_alias_arn" {
  value = module.logging_destination.kms_alias_arn
}

output "logging_destination_kms_alias_target_key_arn" {
  value = module.logging_destination.kms_alias_target_key_arn
}

#####
# Service IAM User
#####

output "logging_destination_iam_user_arn" {
  value = module.logging_destination.iam_user_arn
}

output "logging_destination_iam_user_name" {
  value = module.logging_destination.iam_user_name
}

output "logging_destination_iam_user_unique_id" {
  value = module.logging_destination.iam_user_unique_id
}

output "logging_destination_iam_user_iam_access_key_id" {
  value     = module.logging_destination.iam_user_iam_access_key_id
  sensitive = true
}

output "logging_destination_iam_user_iam_access_key_secret" {
  value     = module.logging_destination.iam_user_iam_access_key_secret
  sensitive = true
}

#####
# IAM policy
#####

output "logging_destination_iam_policy_read_only_id" {
  value = module.logging_destination.iam_policy_read_only_id
}

output "logging_destination_iam_policy_read_only_arn" {
  value = module.logging_destination.iam_policy_read_only_arn
}

output "logging_destination_iam_policy_read_only_description" {
  value = module.logging_destination.iam_policy_read_only_description
}

output "logging_destination_iam_policy_read_only_name" {
  value = module.logging_destination.iam_policy_read_only_name
}

output "logging_destination_iam_policy_read_only_json" {
  value = module.logging_destination.iam_policy_read_only_json
}

output "logging_destination_iam_policy_full_id" {
  value = module.logging_destination.iam_policy_full_id
}

output "logging_destination_iam_policy_full_arn" {
  value = module.logging_destination.iam_policy_full_arn
}

output "logging_destination_iam_policy_full_description" {
  value = module.logging_destination.iam_policy_full_description
}

output "logging_destination_iam_policy_full_name" {
  value = module.logging_destination.iam_policy_full_name
}

output "logging_destination_iam_policy_full_json" {
  value = module.logging_destination.iam_policy_full_json
}

output "logging_destination_iam_policy_data_ro_id" {
  value = module.logging_destination.iam_policy_data_ro_id
}

output "logging_destination_iam_policy_data_ro_arn" {
  value = module.logging_destination.iam_policy_data_ro_arn
}

output "logging_destination_iam_policy_data_ro_description" {
  value = module.logging_destination.iam_policy_data_ro_description
}

output "logging_destination_iam_policy_data_ro_name" {
  value = module.logging_destination.iam_policy_data_ro_name
}

output "logging_destination_iam_policy_data_ro_json" {
  value = module.logging_destination.iam_policy_data_ro_json
}

output "logging_destination_iam_policy_data_rw_id" {
  value = module.logging_destination.iam_policy_data_rw_id
}

output "logging_destination_iam_policy_data_rw_arn" {
  value = module.logging_destination.iam_policy_data_rw_arn
}

output "logging_destination_iam_policy_data_rw_description" {
  value = module.logging_destination.iam_policy_data_rw_description
}

output "logging_destination_iam_policy_data_rw_name" {
  value = module.logging_destination.iam_policy_data_rw_name
}

output "logging_destination_iam_policy_data_rw_json" {
  value = module.logging_destination.iam_policy_data_rw_json
}

output "logging_destination_iam_policy_kms_ro_json" {
  value = module.logging_destination.iam_policy_kms_ro_json
}

output "logging_destination_iam_policy_kms_data_ro_json" {
  value = module.logging_destination.iam_policy_kms_data_ro_json
}

output "logging_destination_iam_policy_kms_rw_json" {
  value = module.logging_destination.iam_policy_kms_rw_json
}

output "logging_destination_iam_policy_kms_data_rw_json" {
  value = module.logging_destination.iam_policy_kms_data_rw_json
}

5.0.0
=====

* chore: (BREAKING) pins AWS provider to 4+
* feat: (BREAKING) convert `var.request_payer` to new `aws_s3_bucket_request_payment_configuration` resource
* feat: (BREAKING) remove `var.sse_config` to new `var.sse_enabled` toggle, simplifying the encryption options
* refactor: (BREAKING) change `source_json` to `source_policy_documents`
* refactor: (BREAKING) moves encryption settings to its own resource
* refactor: (BREAKING) change `apply_bucket_policy` to `bucket_policy_enabled`
* refactor: (BREAKING) moves ACL settings to its own resource
* refactor: (BREAKING) moves versioning settings to its own resource
* refactor: (BREAKING) moves logging settings to its own resource
* refactor: (BREAKING) moves website settings to its own resource
* refactor: (BREAKING) moves lifecycle settings to its own resource
* refactor: (BREAKING) moves cors settings to its own resource
* refactor: (BREAKING) moves object_block_configuration settings to its own resource
* refactor: (BREAKING) refactor `versioning_config` in new `versioning_configuration`, with different content
* refactor: (BREAKING) refactor `logging` in new `logging_configuration`, with `grantee` new option content
* refactor: (BREAKING) refactor `static_website` in new `website_configuration`, with different content
* refactor: (BREAKING) refactor `lifecycle_rules` in new `lifecycle_configuration`, with different content
* refactor: (BREAKING) refactor `var.object_lock_configuration` with different content

4.0.1
=====

* fix: fixes conditions to create policies for S3 user
* chore: pre-commit dependencies update

4.0.0
=====

* feat: adds the ability to create a service user
* feat: adds to add one or more CNAME records linked to the bucket
* feat: nullify outputs that return nothing = null instead of empty string
* feat: outputs JSON for policies, controlled by a new `var.iam_policy_output_jsons`
* feat: (BREAKING) removes `***_descriptions` variables for policies and change them with default values
* feat: adds KMS key JSON for KMS policies
* refactor: (BREAKING) rename `policy_objects_` to `policy_data_` to standardize
* test: changes `var.aws_assume_role` to `var.aws_assume_role_arn`
* chore: removes waiting time for S3 creation by forcing provider 3.73+
* chore: pre-commit dependencies update

3.2.0
=====

* feat: add two policy to be used by users who just need to manipulate objects

3.1.0
=====

* refactor: removes the `count` on the bucket as the resource will always exists.
* feat: enable type `optional` experimental feature

3.0.1
=====

* fix: makes sure KMS key is indeed linked to the bucket when `var.sse_config` is empty

3.0.0
=====

* feat: (BREAKING) required at least Terraform 1.0.1+ and AWS provider 3+
* feat: (BREAKING) if `var.kms_key_create` is `true`, key is used
* feat: (BREAKING) if `var.sse_config` now requires `sse_algorithm` and `kms_master_key_id` optionally, not `sse_key` anymore
* feat: (BREAKING) removes `iam_policy_*_policy_document` output as it breaks idempotency and it’s not a critical output
* feat: (BREAKING) removes `var.apply_kms_policy` as it is not used anymore
* feat: (BREAKING) removes `var.enabled` as it is not needed anymore since Terraform 0.13+
* feat: handle tags for policies and add `var.policy_tags` to make it possible to override them
* feat: handles bucket object ownership with new variable `var.bucket_object_ownership`
* fix: makes sure polices also allow KMS alias, not only KMS key
* refactor: use `managed-by`=`Terraform` instead of deprecated `Terraform=true`
* refactor: adds `origin` tag
* refactor: moves Policies data source to `data.tf`
* chore: pre-commit configuration update
* chore: removes pre-commit afcmf as it is handled by gitlab
* chore: aligns .gitignore with modern terraform practices
* test: removes Jenkinsfile
* test: use provider/variables that fits most CI/CD
* test: fixes `MalformedPolicyDocumentException: The new key policy will not allow you to update the key policy in the future` error
* test: removes output descriptions so the module is easier to maintain
* doc: adds LICENSE
* doc: refactors `CHANGELOG.md` to align with most changelogs

2.1.0
=====

  * feat: add kms_key_rotation_enabled input:
    * add support for kms key rotation.
    * kms_key_rotation_enabled input variable defaults to true for best security practice.
  * test: update examples:
    * add standard s3 sse_config in test examples/default/standard-versionned.tf.
    * add sse_config with custom kms key in test examples/default/external-kms-no-policies.tf.
  * chore: upgrade version and required_providers: upgrade to terraform 0.12.26.

2.0.0
=====

  * fix: (BREAKING) remove region from bucket definition (needed for aws provider > 3) and `variables.tf`
  * fix: permit aws provider > 3
  * fix: count on data resources (unbreak when the KMS key is used inside `sse_config`)
  * chore: bump pre-commit hooks to fix jenkins test

1.0.3
=====

  * fix: null value when s3 bucket is not created

1.0.2
=====

  * fix: wrong IAM polices when default bucket KMS key is used

1.0.1
=====

  * fix: pre-commit failure due to version bumps

1.0.0
=====

  * breaking: terraform 0.12 upgrade and new features

0.4.0
=====

  * Merge branch 'feature/kms_policy' of fxinnovation-public/terraform-module-aws-bucket-s3 into master
  * fix/ typo
  * feat/ KMS key policy suport

0.3.0
=====

  * Merge branch 'feature/missing_outputs_add_lifecycle' of fxinnovation-public/terraform-module-aws-bucket-s3 into master
  * fix/ typo
  * fix/ output + remove prefix
  * fix/ typo
  * fix/ outputs
  * tech/ changelog
  * feature/ add object_lock expiration time

0.2.0
=====

  * Merge branch 'feature/add_object_lock_and_versioning' of fxinnovation-public/terraform-module-aws-bucket-s3 into master
  * fix/ typo
  * tech/ CHANGELOG
  * feature/ add versioning and object_lock

0.1.0
=====

  * Merge branch 'feature/init' of fxinnovation-public/terraform-module-aws-bucket-s3 into master
  * fix/ output format
  * tech/ bump pre-commit version
  * fix/ stupid terraform 0.11
  * fix/ data source
  * fix/ bucket policy
  * fix/ tests
  * feature/ add disable test
  * fix/ descriptions
  * fix/ typo
  * fix/ var name
  * fix/ missing comma
  * fix/ pre-commit version
  * fix/ missing output and missing pre-commit
  * feature/ init
  * Initial commit

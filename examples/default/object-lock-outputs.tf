#####
# S3 bucket
#####

output "object_lock_with_policy_id" {
  value = module.object_lock_with_policy.id
}

output "object_lock_with_policy_arn" {
  value = module.object_lock_with_policy.arn
}

output "object_lock_with_policy_bucket_domain_name" {
  value = module.object_lock_with_policy.bucket_domain_name
}

output "object_lock_with_policy_bucket_regional_domain_name" {
  value = module.object_lock_with_policy.bucket_regional_domain_name
}

output "object_lock_with_policy_hosted_zone_id" {
  value = module.object_lock_with_policy.hosted_zone_id
}

output "object_lock_with_policy_region" {
  value = module.object_lock_with_policy.region
}

#####
# KMS key
#####

output "object_lock_with_policy_kms_key_arn" {
  value = module.object_lock_with_policy.kms_key_arn
}

output "object_lock_with_policy_kms_key_id" {
  value = module.object_lock_with_policy.kms_key_id
}

output "object_lock_with_policy_kms_alias_arn" {
  value = module.object_lock_with_policy.kms_alias_arn
}

output "object_lock_with_policy_kms_alias_target_key_arn" {
  value = module.object_lock_with_policy.kms_alias_target_key_arn
}

#####
# Service IAM User
#####

output "object_lock_with_policy_iam_user_arn" {
  value = module.object_lock_with_policy.iam_user_arn
}

output "object_lock_with_policy_iam_user_name" {
  value = module.object_lock_with_policy.iam_user_name
}

output "object_lock_with_policy_iam_user_unique_id" {
  value = module.object_lock_with_policy.iam_user_unique_id
}

output "object_lock_with_policy_iam_user_iam_access_key_id" {
  value     = module.object_lock_with_policy.iam_user_iam_access_key_id
  sensitive = true
}

output "object_lock_with_policy_iam_user_iam_access_key_secret" {
  value     = module.object_lock_with_policy.iam_user_iam_access_key_secret
  sensitive = true
}

#####
# IAM policy
#####

output "object_lock_with_policy_iam_policy_read_only_id" {
  value = module.object_lock_with_policy.iam_policy_read_only_id
}

output "object_lock_with_policy_iam_policy_read_only_arn" {
  value = module.object_lock_with_policy.iam_policy_read_only_arn
}

output "object_lock_with_policy_iam_policy_read_only_description" {
  value = module.object_lock_with_policy.iam_policy_read_only_description
}

output "object_lock_with_policy_iam_policy_read_only_name" {
  value = module.object_lock_with_policy.iam_policy_read_only_name
}

output "object_lock_with_policy_iam_policy_read_only_json" {
  value = module.object_lock_with_policy.iam_policy_read_only_json
}

output "object_lock_with_policy_iam_policy_full_id" {
  value = module.object_lock_with_policy.iam_policy_full_id
}

output "object_lock_with_policy_iam_policy_full_arn" {
  value = module.object_lock_with_policy.iam_policy_full_arn
}

output "object_lock_with_policy_iam_policy_full_description" {
  value = module.object_lock_with_policy.iam_policy_full_description
}

output "object_lock_with_policy_iam_policy_full_name" {
  value = module.object_lock_with_policy.iam_policy_full_name
}

output "object_lock_with_policy_iam_policy_full_json" {
  value = module.object_lock_with_policy.iam_policy_full_json
}

output "object_lock_with_policy_iam_policy_data_ro_id" {
  value = module.object_lock_with_policy.iam_policy_data_ro_id
}

output "object_lock_with_policy_iam_policy_data_ro_arn" {
  value = module.object_lock_with_policy.iam_policy_data_ro_arn
}

output "object_lock_with_policy_iam_policy_data_ro_description" {
  value = module.object_lock_with_policy.iam_policy_data_ro_description
}

output "object_lock_with_policy_iam_policy_data_ro_name" {
  value = module.object_lock_with_policy.iam_policy_data_ro_name
}

output "object_lock_with_policy_iam_policy_data_ro_json" {
  value = module.object_lock_with_policy.iam_policy_data_ro_json
}

output "object_lock_with_policy_iam_policy_data_rw_id" {
  value = module.object_lock_with_policy.iam_policy_data_rw_id
}

output "object_lock_with_policy_iam_policy_data_rw_arn" {
  value = module.object_lock_with_policy.iam_policy_data_rw_arn
}

output "object_lock_with_policy_iam_policy_data_rw_description" {
  value = module.object_lock_with_policy.iam_policy_data_rw_description
}

output "object_lock_with_policy_iam_policy_data_rw_name" {
  value = module.object_lock_with_policy.iam_policy_data_rw_name
}

output "object_lock_with_policy_iam_policy_data_rw_json" {
  value = module.object_lock_with_policy.iam_policy_data_rw_json
}

output "object_lock_with_policy_iam_policy_kms_ro_json" {
  value = module.object_lock_with_policy.iam_policy_kms_ro_json
}

output "object_lock_with_policy_iam_policy_kms_data_ro_json" {
  value = module.object_lock_with_policy.iam_policy_kms_data_ro_json
}

output "object_lock_with_policy_iam_policy_kms_rw_json" {
  value = module.object_lock_with_policy.iam_policy_kms_rw_json
}

output "object_lock_with_policy_iam_policy_kms_data_rw_json" {
  value = module.object_lock_with_policy.iam_policy_kms_data_rw_json
}

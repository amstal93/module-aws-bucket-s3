#####
# Providers
#####

provider "aws" {
  region     = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile

  assume_role {
    role_arn = var.aws_assume_role
  }
}

#####
# Randoms
#####

resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
  number  = false
}

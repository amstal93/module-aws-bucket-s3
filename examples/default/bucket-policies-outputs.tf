#####
# S3 bucket
#####

output "policy_id" {
  value = module.policy.id
}

output "policy_arn" {
  value = module.policy.arn
}

output "policy_bucket_domain_name" {
  value = module.policy.bucket_domain_name
}

output "policy_bucket_regional_domain_name" {
  value = module.policy.bucket_regional_domain_name
}

output "policy_hosted_zone_id" {
  value = module.policy.hosted_zone_id
}

output "policy_region" {
  value = module.policy.region
}

#####
# KMS key
#####

output "policy_kms_key_arn" {
  value = module.policy.kms_key_arn
}

output "policy_kms_key_id" {
  value = module.policy.kms_key_id
}

output "policy_kms_alias_arn" {
  value = module.policy.kms_alias_arn
}

output "policy_kms_alias_target_key_arn" {
  value = module.policy.kms_alias_target_key_arn
}

#####
# Service IAM User
#####

output "policy_iam_user_arn" {
  value = module.policy.iam_user_arn
}

output "policy_iam_user_name" {
  value = module.policy.iam_user_name
}

output "policy_iam_user_unique_id" {
  value = module.policy.iam_user_unique_id
}

output "policy_iam_user_iam_access_key_id" {
  value     = module.policy.iam_user_iam_access_key_id
  sensitive = true
}

output "policy_iam_user_iam_access_key_secret" {
  value     = module.policy.iam_user_iam_access_key_secret
  sensitive = true
}

#####
# IAM policy
#####

output "policy_iam_policy_read_only_id" {
  value = module.policy.iam_policy_read_only_id
}

output "policy_iam_policy_read_only_arn" {
  value = module.policy.iam_policy_read_only_arn
}

output "policy_iam_policy_read_only_description" {
  value = module.policy.iam_policy_read_only_description
}

output "policy_iam_policy_read_only_name" {
  value = module.policy.iam_policy_read_only_name
}

output "policy_iam_policy_read_only_json" {
  value = module.policy.iam_policy_read_only_json
}

output "policy_iam_policy_full_id" {
  value = module.policy.iam_policy_full_id
}

output "policy_iam_policy_full_arn" {
  value = module.policy.iam_policy_full_arn
}

output "policy_iam_policy_full_description" {
  value = module.policy.iam_policy_full_description
}

output "policy_iam_policy_full_name" {
  value = module.policy.iam_policy_full_name
}

output "policy_iam_policy_full_json" {
  value = module.policy.iam_policy_full_json
}

output "policy_iam_policy_data_ro_id" {
  value = module.policy.iam_policy_data_ro_id
}

output "policy_iam_policy_data_ro_arn" {
  value = module.policy.iam_policy_data_ro_arn
}

output "policy_iam_policy_data_ro_description" {
  value = module.policy.iam_policy_data_ro_description
}

output "policy_iam_policy_data_ro_name" {
  value = module.policy.iam_policy_data_ro_name
}

output "policy_iam_policy_data_ro_json" {
  value = module.policy.iam_policy_data_ro_json
}

output "policy_iam_policy_data_rw_id" {
  value = module.policy.iam_policy_data_rw_id
}

output "policy_iam_policy_data_rw_arn" {
  value = module.policy.iam_policy_data_rw_arn
}

output "policy_iam_policy_data_rw_description" {
  value = module.policy.iam_policy_data_rw_description
}

output "policy_iam_policy_data_rw_name" {
  value = module.policy.iam_policy_data_rw_name
}

output "policy_iam_policy_data_rw_json" {
  value = module.policy.iam_policy_data_rw_json
}

output "policy_iam_policy_kms_ro_json" {
  value = module.policy.iam_policy_kms_ro_json
}

output "policy_iam_policy_kms_data_ro_json" {
  value = module.policy.iam_policy_kms_data_ro_json
}

output "policy_iam_policy_kms_rw_json" {
  value = module.policy.iam_policy_kms_rw_json
}

output "policy_iam_policy_kms_data_rw_json" {
  value = module.policy.iam_policy_kms_data_rw_json
}

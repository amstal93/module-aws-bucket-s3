#####
# Standard versionned example
#####

module "logging_destination" {
  source = "../../"

  name = "tftestloggingdest${random_string.this.result}"
  acl  = "log-delivery-write"

  kms_key_create          = false
  sse_enabled             = false
  iam_policy_create       = true
  iam_policy_read_name    = "tftestloggingdestread${random_string.this.result}"
  iam_policy_full_name    = "tftestloggingdestfull${random_string.this.result}"
  iam_policy_data_ro_name = "tftestoggingdestro${random_string.this.result}"
  iam_policy_data_rw_name = "tftestoggingdestrw${random_string.this.result}"

  tags = {
    testTag = "tftest"
    module  = "standard"
  }
}

module "logging" {
  source = "../../"

  name = "tftestlogging${random_string.this.result}"

  iam_policy_create       = true
  iam_policy_read_name    = "tftestloggingread${random_string.this.result}"
  iam_policy_full_name    = "tftestloggingfull${random_string.this.result}"
  iam_policy_data_ro_name = "tftestloggingro${random_string.this.result}"
  iam_policy_data_rw_name = "tftestloggingrw${random_string.this.result}"

  logging_configuration = {
    target_bucket = module.logging_destination.id
    target_prefix = "tftest"
  }

  tags = {
    testTag = "tftest"
    module  = "standard"
  }
}

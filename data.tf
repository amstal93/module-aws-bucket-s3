#####
# IAM policy
#####

locals {
  iam_policy_required = var.iam_user_enabled || var.iam_policy_create || var.iam_policy_output_jsons
}

data "aws_iam_policy_document" "kms_read" {
  count = local.iam_policy_required ? 1 : 0

  statement {
    sid = "KMSS3ReadAll"

    effect = "Allow"

    actions = [
      "kms:Describe*",
      "kms:Get*",
    ]

    resources = compact([
      local.kms_key_arn,
      var.kms_key_create ? aws_kms_key.this.*.arn[0] : var.kms_key_arn,
    ])
  }

  statement {
    sid = "KmsS3ListAll"

    effect = "Allow"

    actions = [
      "kms:List*",
    ]

    resources = ["*"]
  }
}

data "aws_iam_policy_document" "kms_read_data" {
  count = local.iam_policy_required ? 1 : 0

  source_policy_documents = [data.aws_iam_policy_document.kms_read.*.json[0]]

  statement {
    sid = "S3KmsDataResourceRead"

    effect = "Allow"

    actions = [
      "kms:Decrypt",
      "kms:Verify",
    ]

    resources = compact([
      local.kms_key_arn,
      var.kms_key_create ? aws_kms_key.this.*.arn[0] : var.kms_key_arn,
    ])
  }

  statement {
    sid = "S3KmsDataList"

    effect = "Allow"

    actions = [
      "kms:ListAliases",
      "kms:ListKeys",
    ]

    resources = ["*"]
  }
}

data "aws_iam_policy_document" "kms_write_data" {
  count = local.iam_policy_required ? 1 : 0

  source_policy_documents = [data.aws_iam_policy_document.kms_read_data.*.json[0]]

  statement {
    sid = "S3KmsDataResourceWrite"

    effect = "Allow"

    actions = [
      "kms:Encrypt",
      "kms:ReEncryptTo",
      "kms:ReEncryptFrom",
      "kms:ReplicateKey",
      "kms:Sign",
      "kms:EnableKey",
      "kms:DisableKey",
    ]

    resources = compact([
      local.kms_key_arn,
      var.kms_key_create ? aws_kms_key.this.*.arn[0] : var.kms_key_arn,
    ])
  }
}

data "aws_iam_policy_document" "kms_full" {
  count = local.iam_policy_required ? 1 : 0

  statement {
    sid = "KMSS3DataFull"

    effect = "Allow"

    actions = [
      "kms:*",
    ]

    resources = compact([
      local.kms_key_arn,
      var.kms_key_create ? aws_kms_key.this.*.arn[0] : var.kms_key_arn,
    ])
  }
}

data "aws_iam_policy_document" "this_read" {
  count = local.iam_policy_required ? 1 : 0

  source_policy_documents = local.use_kms_key ? [element(concat(data.aws_iam_policy_document.kms_read_data.*.json, [""]), 0)] : null

  statement {
    sid = "BucketRead"

    effect = "Allow"

    actions = [
      "s3:Get*",
      "s3:List*",
    ]

    resources = [
      aws_s3_bucket.this.arn,
      "${aws_s3_bucket.this.arn}/*",
    ]
  }

  statement {
    sid = "ListBucket"

    effect = "Allow"

    actions = [
      "s3:ListAllMyBuckets",
    ]

    resources = ["*"]
  }
}

data "aws_iam_policy_document" "this_full" {
  count = local.iam_policy_required ? 1 : 0

  source_policy_documents = local.use_kms_key ? [element(concat(data.aws_iam_policy_document.kms_full.*.json, [""]), 0)] : null

  statement {
    sid = "S3FullAccess"

    effect = "Allow"

    actions = [
      "s3:*",
    ]

    resources = [
      aws_s3_bucket.this.arn,
      "${aws_s3_bucket.this.arn}/*",
    ]
  }

  statement {
    sid = "ListBucket"

    effect = "Allow"

    actions = [
      "s3:ListAllMyBuckets",
    ]

    resources = ["*"]
  }
}

data "aws_iam_policy_document" "this_data_rw" {
  count = local.iam_policy_required ? 1 : 0

  source_policy_documents = local.use_kms_key ? [element(concat(data.aws_iam_policy_document.kms_full.*.json, [""]), 0)] : null

  statement {
    sid = "S3ObjectsRwAccess"

    effect = "Allow"

    actions = [
      "s3:*Object",
      "s3:ListBucket"
    ]

    resources = [
      aws_s3_bucket.this.arn,
      "${aws_s3_bucket.this.arn}/*",
    ]
  }

  statement {
    sid = "ListBuckets"

    effect = "Allow"

    actions = [
      "s3:ListAllMyBuckets",
    ]

    resources = ["*"]
  }
}

data "aws_iam_policy_document" "this_data_ro" {
  count = local.iam_policy_required ? 1 : 0

  source_policy_documents = local.use_kms_key ? [element(concat(data.aws_iam_policy_document.kms_write_data.*.json, [""]), 0)] : null

  statement {
    sid = "S3ObjectsROAccess"

    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:ListBucket"
    ]

    resources = [
      aws_s3_bucket.this.arn,
      "${aws_s3_bucket.this.arn}/*",
    ]
  }

  statement {
    sid = "ListBuckets"

    effect = "Allow"

    actions = [
      "s3:ListAllMyBuckets",
    ]

    resources = ["*"]
  }
}
